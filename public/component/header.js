
import 'bootstrap/dist/css/bootstrap.css'
import styles from '../../styles/Home.module.css'
import Head from 'next/head'
import React, { useState, useEffect } from 'react';

const HeaderWbook = () => {
    const [authorSession, setAuthorSession] = useState(null);
    const [author, setAuthor] = useState(null);
    useEffect(() => {
        setAuthorSession(sessionStorage.getItem('author'))
    })

    if (JSON.parse(authorSession)?.userName) {
        if (JSON.parse(authorSession)?.userName !== null) {

            if (author === null) {
                setAuthor(JSON.parse(authorSession));
            }

        }

    }




    return (
        <div className={styles.container}>
            <Head>
                <title>Wbook</title>
                <meta name="description" content="" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className={styles.main}>

                <h1 className={styles.title}>
                    Wbook
                </h1>
            </main>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#"></a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="/">Accueil</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/pageAuthor">{author?.userName != null ? author?.userName : 'Connexion/inscription'}</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Vos livres</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/pageNewBook">Creer/importer un livre</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Recherchez un livre</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">{author?.userName != null ? 'Se déconnecter' : ''}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default HeaderWbook;