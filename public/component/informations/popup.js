import styles from '../../../styles/Home.module.css'
import React, { useState } from 'react';

const Popup = ({ view, message, status }) => {
    const [popup, setPopup] = useState(view);
    const [messages, setMessages] = useState(message);
    const [statu, setStatu] = useState(status);
     function close() {
       setPopup(false)
   
    }
    if (popup === true) {
        console.log( messages)
        return (
            <div  className={styles.container_body}>
                <h1>{status === "OK"?'Succés': 'Echec'}</h1>
                <p>{status === "K0"? messages: 'Opération abouti'}</p>
                <button onClick={close} {...view == false}>Fermé</button>
            </div>
        );
    } else {
        <div></div>
    }



}

export default Popup;