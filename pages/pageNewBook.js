import React from "react"
import HeaderWbook from "../public/component/header"
import styles from '../styles/Home.module.css'
import AddBook from "../public/component/addBook"

const PageNewBook = () => {
    return (
        <div>
            <HeaderWbook></HeaderWbook>

            <div className={styles.container_body}>
                <div className="btn-group center d-flex justify-content-center">
                    <a href="#"  className="btn btn-primary active" aria-current="page">Avec fichier</a>
                    <a href="#" className="btn btn-primary">A écrire</a>
                </div>
                <div>
                    <AddBook></AddBook>
                </div>
                <div>

                </div>

            </div>
        </div>
    )
}

export default PageNewBook;