import React from "react"
import HeaderWbook from "../public/component/header"
import AddAuthor from "../public/component/addAuthor"
import styles from '../styles/Home.module.css'


const PageAuthor = props => {
  return (
   <div>
    <HeaderWbook></HeaderWbook>
    <div className={styles.container_body}>
    <AddAuthor></AddAuthor>
    </div>
    
   </div>
  )
}
export default PageAuthor