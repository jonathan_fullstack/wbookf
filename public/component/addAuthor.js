import AuthorService from "../../pages/services/authorService";
import React, { useState, useEffect } from 'react';
import Popup from '../component/informations/popup'
import Head from 'next/head'

export default function PageWithJSbasedForm() {
   const [error, setError] = useState(0);
   const [errorMessage, setErrorMessage] = useState("");
   const [popup, setPopup] = useState(false);
   const [status, setStatus] = useState("");
   const [authorSession, setAuthorSession] = useState(null);
   // Handles the submit event on form submit.
   const handleSubmit = async (event) => {
      // Stop the form from submitting and refreshing the page.
      event.preventDefault()

      // Get data from the form.
      const author = {
         firstName: event.target.firstName,
         lastName: event.target.lastName,
         userName: event.target.userName,
         email: event.target.email,
         password: event.target.password,
      }

      function validator(params) {
         const firstName = document.querySelector('#firstName').value;
         const lastName = document.querySelector('#lastName').value;
         const userName = document.querySelector('#userName').value;
         const email = document.querySelector('#email').value;
         const password = document.querySelector('#password').value;

         if (!firstName) {
            alert("Le prénom  est obligatoire !")
         } else {
            author.firstName = firstName;
         }

         if (!lastName) {
            alert("le nom est obligatoire !")
         } else {
            author.lastName = lastName;
         }
         if (!userName) {
            author.userName = firstName + "-" + lastName;
         }

         if (!email) {
            alert("Ce champs est obligatoire !")
         } else {
            author.email = email;
         }

         if (!password) {
            alert("Ce champs est obligatoire !");
            if (password.length < 6) {
               alert("Le mot de passe doit faire au minimum 6 caracteres");
            }
         } else {
            author.password = password;
         }
         return author;
      }

      // Send the data to the server in JSON format.
      const result = await AuthorService.create(validator()).then(res => {
         setStatus("OK");
         console.log(res.data)
        
               sessionStorage.setItem('author', JSON.stringify(res.data))
         
         
         // useEffect(() => {
         //    setAuthorSession(sessionStorage.setItem('author', JSON.stringify(res.data)))
         //        })
      }).catch(err => {
            console.log(err);
            if (err) {
               setError(err?.response?.status)
            setErrorMessage(err?.response?.data)
            }
            
         });
      if (error = 506) {
         setPopup(true);
         setStatus("K0");
      }

   }
   return (


      // We pass the event to the handleSubmit() function on submit.
      <form  className="row g-3">

         <div className="col-md-12">
            {error === 506 ? <Popup view={popup} message={errorMessage} status={status}></Popup> : <div></div>}
         </div>

         <div className="col-md-4">
            <label htmlFor="firstName" className="form-label">Prénom</label>
            <input type="text" id="firstName" name="firstName" className="form-control" required />
         </div>

         <div className="col-md-4">
            <label htmlFor="lastName" className="form-label">Nom</label>
            <input type="text" id="lastName" name="lastName" className="form-control" required />
         </div>

         <div className="col-md-4">
            <label htmlFor="userName" className="form-label">Pseudo</label>
            <input type="text" id="userName" name="userName" className="form-control" />
         </div>

         <div className="col-md-4">
            <label htmlFor="email" className="form-label">Email</label>
            <input type="text" id="email" name="email" className="form-control" required />
         </div>

         <div className="col-md-4"></div>

         <div className="col-md-4">
            <label htmlFor="password" className="form-label">Mot de passe</label>
            <input type="text" id="password" name="password" className="form-control" required />
         </div>

         <button onClick={handleSubmit}>Enregistrer</button>
      </form>
   )
}