import Head from 'next/head'
import Image from 'next/image'
import ListBook from '../public/component/listBook'
import styles from '../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.css'
import AddAuthor from '../public/component/addAuthor'
import PageAuthor from './pageAuthor'
import HeaderWbook from '../public/component/header'

export default function Home() {

  return (
    <div>  
      <HeaderWbook></HeaderWbook>
  
  <div className={styles.container_body}>
      <ListBook></ListBook>
  </div>
  </div>
      
  )
}
