import styles from '../../styles/ContainerList.module.css'
import 'bootstrap/dist/css/bootstrap.css'

const ListBook = () => {
    const data = [{ "name": "test1" }, { "name": "test2" },];
    return (
        <div className={styles.containerList}>
            <div className={styles.container}>
                    {data.map(function (d, idx) {
                        return (
                        <div className={styles.containerP}  key={idx}>
                            <p>{d.name}</p>
                            <button className="btn btn-primary ">Voir</button>
                            <button className="btn btn-primary  ">Modifier</button>
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}

export default ListBook;