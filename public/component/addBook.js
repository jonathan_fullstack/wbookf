import React, { useState, useEffect } from 'react';
import Popup from '../component/informations/popup'
import BookService from '../../pages/services/bookService';
import FormData from "form-data";

const AddBook = () => {
   const [error, setError] = useState(null);
   const [errorMessage, setErrorMessage] = useState("");
   const [popup, setPopup] = useState(false);
   const [status, setStatus] = useState("");
   const [authorSession, setAuthorSession] = useState(null);

   const [image, setImage] = useState(null);
   const [isValid, setIsValid] = useState(null);

   const [name, setName] = useState(null);
   const [resume, setResume] = useState(null);
   const [body, setBody] = useState(null);
   const [categoryBook, setCatgoryBook] = useState(null);
   const [author, setAuthor] = useState(null);

   const formData = new FormData();


   useEffect(() => {
      setAuthorSession(sessionStorage.getItem('author'))
   })

   if (JSON.parse(authorSession)?.body) {
      if (JSON.parse(authorSession)?.body !== null) {

         if (author === null) {
            setAuthor(JSON.parse(authorSession));
         }

      }

   }

   const handleChange = event => {

      if (event.target.files && event.target.files[0]) {
         const i = event.target.files[0];
         setBody(i);
      }
   };

   const validator = () => {
      if (!name) {
         console.log("le titre est obligatoire");
      } else {
         if (!resume) {
            console.log('le resume est obligatoire');
         } else {
            if (!body) {
               console.log('le contenu est obligatoire');
            } else {
               if (!author) {
                  console.log('Vous êtes pas inscris');
               } else {
                  setIsValid(true);
               }
            }
         }
      }






      if (!categoryBook) {
         console.log('La catégorie est authomatique');
      } else {
         setIsValid(true);
      }
   }

   const handleSubmit = async (event) => {
      // Stop the form from submitting and refreshing the page.
      event.preventDefault();
      const valName = document.querySelector('#name').value;
      const valResume = document.querySelector('#resume').value;
      formData.append("name", valName);
      formData.append("resume", valResume);
      formData.append("authorDto", "JONATHAN-DE LA OSA");
      formData.append("body", body);
      formData.append("categoryBook", document.querySelector("#categoryBook").value);
      if (formData.get('name') == null) {
         alert("titre pas present")
      } else {
         if (formData.get('resume') == null) {
            alert("resume pas present")
         } else {
            if (formData.get('body') == null) {
               alert("contenu pas present")
            } else {
               if (formData.get('authorDto') == null) {
                  alert("authorDto pas present")
               } else {
                  if (formData.get('categoryBook') == null) {
                     alert("categoryBook pas present")
                  } else {
                     BookService.creates(formData).then(res => {
                        setStatus("OK");
                        setPopup(true);
                        setError(200)
                        setErrorMessage("Livre ajouté avec succés")
                     }).catch(err => {
                        setError(err.response.status)
                        setErrorMessage(err.response.message)
                        setIsValid(err.response.status)
                        if (err.response.status = 506) {
                           setPopup(true);
                           setStatus("K0");
                        } else {
                           if (err.response.status = 400) {
                              setPopup(true);
                              setStatus("K0");
                  
                           } else {
                              if (err.response.status = 500) {
                                 setPopup(true);
                                 setStatus("K0");
                  
                              }
                           }
                        }
                     });
                  }
               }
            }
      
         }
      }
      

     
   

   }




   return (
      // We pass the event to the handleSubmit() function on submit.
      <form className="row g-3">

         <div className="col-md-12">
            {isValid === 506 ? <Popup view={popup} message={errorMessage} status={status}></Popup> : <div></div>}
            {isValid === 500 ? <Popup view={popup} message={errorMessage} status={status}></Popup> : <div></div>}
            {isValid === 400 ? <Popup view={popup} message={errorMessage} status={status}></Popup> : <div></div>}
            {status === "OK" ? <Popup view={popup} message={errorMessage} status={status}></Popup> : <div></div>}
         </div>

         <div className="col-md-8">
            <label htmlFor="name" className="form-label">Titre</label>
            <input type="text" id="name" name="name" className="form-control" required />
         </div>


         <div className="input-group col-md-12" >
            <span className="input-group-text">Description</span>
            <textarea className="form-control" aria-label="With textarea" id="resume" name="resume" required></textarea>
         </div>

         <select className="form-select" aria-label="Default select example" id='categoryBook'>
            <option selected>Choisir une catégorie</option>
            <option value="ROMANCE" defaultValue={"ROMANCE"}>Romance</option>
            <option value="THRILLER">thriler</option>
            <option value="POLITIC">Politique</option>
            <option value="EROTIC">Erotique</option>
            <option value="CHILD">Enfance</option>
            <option value="POESIE">Poésie</option>
         </select>

         <div className="input-group col-md-12">
            <label htmlFor="body" className="form-label"  >Importer votre livre</label>
            <input type="file" id="body" name="body" className="form-control" onChange={handleChange} />
         </div>

         <button onClick={handleSubmit}>Création du livre</button>
      </form>
   )
};

export default AddBook